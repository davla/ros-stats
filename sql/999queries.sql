create or replace view languages_with_file as
select project, language, right(file, -43 - length(project)) as file, nBlank,
    nComment, nCode, nScaled
from t;

create or replace view languages as
select project, language, count(*) as nFiles, sum(nBlank) as nBlank,
    sum(nComment) as nComment, sum(nCode) as nCode, sum(nScaled) as nScaled
from t
where
    language not like 'C%'
    or language in ('CMake')
group by project, language
union
select project, 'C++', count(*) as nFiles, sum(nBlank) as nBlank,
    sum(nComment) as nComment, sum(nCode) as nCode, sum(nScaled) as nScaled
from t
where
    language like 'C%'
    and language not in ('CMake')
group by project
order by project, nFiles desc;

create or replace view languages_whole_perc as
select language,
    cast(sum(nCode) as decimal) /
        (select sum(nCode) from languages) * 100 as loc_perc,
    cast(sum(nFiles) as decimal) /
        (select sum(nFiles) from languages) * 100 as files_perc,
    cast(count(*) as decimal) /
        (select count(distinct project) from languages) * 100 as projects_perc
from languages
group by language
order by loc_perc desc, files_perc desc;

create or replace view language_projects_perc as
select l0.project, l0.language,
    cast(sum(nCode) as decimal) / project_loc * 100 as loc_perc,
    cast(sum(nFiles) as decimal) / project_files * 100 as files_perc
from languages as l0
    join
        (select project,
            sum(nCode) as project_loc,
            sum(nFiles) as project_files
        from languages
        group by project) as l1
    on l0.project = l1.project
group by l0.project, l0.language, project_loc, project_files
order by project, loc_perc desc, files_perc desc;

create or replace view language_whole_per_project as
select lpp.language,
    avg(lpp.loc_perc) as avg_loc, stddev_pop(lpp.loc_perc) as std_loc,
    avg(lpp.files_perc) as avg_files, stddev_pop(lpp.files_perc) as std_files,
    lwp.projects_perc
from language_projects_perc as lpp
    join languages_whole_perc as lwp
        on lpp.language = lwp.language
group by lpp.language, lwp.projects_perc
order by lwp.projects_perc desc, avg_loc desc;

create or replace view plugins_per_package as
select package,
    sum(nIncludes) as nIncludes,
    sum(nClassInstance) as nClassInstance,
    sum(nInstance) as nInstance,
    sum(nUnmanagedInstance) as nUnmanagedInstance,
    cast(sum(nIncludes + nClassInstance + nInstance + nUnmanagedInstance) > 0 as integer) as uses_plugin,
    cast(sum(nIncludes) as decimal) / count(*) * 100 as includes_perc,
    cast(sum(nClassInstance) as decimal) / count(*) * 100 as classInstance_perc,
    cast(sum(nInstance) as decimal) / count(*) * 100 as instance_perc,
    cast(sum(nUnmanagedInstance) as decimal) / count(*) * 100 as unmanagedInstance_perc
from plugins
group by package
order by uses_plugin, package;

create or replace view plugins_whole_perc as
select
    cast(sum(cast(nIncludes > 0 as integer)) as decimal) / count(*) * 100 as includes_perc,
    cast(sum(cast(nClassInstance > 0 as integer)) as decimal) / count(*) * 100 as classInstance_perc,
    cast(sum(cast(nInstance > 0 as integer)) as decimal) / count(*) * 100 as instance_perc,
    cast(sum(cast(nUnmanagedInstance > 0 as integer)) as decimal) / count(*) * 100 as unmanagedInstance_perc,
    cast(sum(uses_plugin) as decimal) / count(*) * 100 as uses_plugin_perc
from plugins_per_package
order by uses_plugin_perc;

select * from plugins_whole_perc;
