#!/usr/bin/env bash

ROOT_DIR="$(dirname ${BASH_SOURCE[0]})"
PACKAGES_DIR="$ROOT_DIR/packages"

RESULTS_FILE='./results.csv'
STAT_FUNCTION=''

while getopts 'O:F:' OPTION; do
	case "$OPTION" in
		'O')
			RESULTS_FILE="$OPTARG"
			;;

		'F')
			STAT_FUNCTION="$OPTARG"
			;;

		*)	# getopts has already printed an error message
			exit 127
			;;
	esac
done

if [[ -z "$STAT_FUNCTION" ]]; then
	echo >&2 'No statistics function given!'
	exit 126
fi

get_git() {
    local URL="$1"
    local TAG="$2"
    local DIR="$3"

    git clone "$URL" "$DIR" &> /dev/null
    [ $? -ne 0 ] && { echo >&2 Error while cloning $URL - $TAG; exit 1; }

    git -C "$DIR" fetch --tags > /dev/null
    [ $? -ne 0 ] && { echo >&2 Error while fetching tags $URL - $TAG; exit 1; }

    git -C "$DIR" checkout "$TAG" > /dev/null
    [ $? -ne 0 ] && { echo >&2 Error while checkout out $URL - $TAG; exit 1; }
}

get_hg() {
    local URL="$1"
    local TAG="$2"
    local DIR="$3"

    hg clone "$URL" "$DIR" &> /dev/null
    [ $? -ne 0 ] && { echo >&2 Error while cloning $URL - $TAG; exit 1; }

    hg --cwd "$DIR" pull > /dev/null
    [ $? -ne 0 ] && { echo >&2 Error while pulling $URL - $TAG; exit 1; }

    hg --cwd "$DIR" update "$TAG" > /dev/null
    [ $? -ne 0 ] && { echo >&2 Error while updating $URL - $TAG; exit 1; }
}

function get-package {
    local PACKAGE="$1"
    local HASH="$2"
    local VCS="$3"
    local URL="$4"
    local PACKAGE_DIR="$PACKAGES_DIR/$PACKAGE-$HASH"

    if [[ ! -d "$PACKAGE_DIR" ]]; then
        get_"$VCS" "$URL" "$HASH" "$PACKAGE_DIR"
    fi

    echo "$PACKAGE_DIR"
}

function publishing {

	echo 'Package,NodeHandle calls,NodeHandle with arguments,advertise,Bare Publisher,Publication,TopicManager,SingleSubscriberPublisher' >> "$RESULTS_FILE"

	while read PACKAGE HASH VCS URL; do
        PACKAGE_DIR="$(get-package "$PACKAGE" "$HASH" "$VCS" "$URL")"

    	echo "Analysing package $PACKAGE"
		local CPP_FILES=$(find "$PACKAGE_DIR" -type f -name '*.cpp')

		if [[ -z "$CPP_FILES" ]]; then
			echo "$PACKAGE,0" >> "$RESULTS_FILE"
			continue
		fi

		local NH_NO=$(grep 'NodeHandle' $CPP_FILES | wc -l)
		local NH_ARGS_NO=$(grep -P 'NodeHandle\s+[_a-zA-Z][_a-zA-Z0-9]*\(' $CPP_FILES | wc -l)
		local ADVERTISE_NO=$(grep 'advertise' $CPP_FILES | wc -l)
		local BARE_PUB_NO=$(grep -P 'Publisher\s+[_a-zA-Z][_a-zA-Z0-9]*\(' $CPP_FILES | wc -l)
		local PUBLICATION_NO=$(grep 'Publication' $CPP_FILES | wc -l)
		local TOPICMANAGER_NO=$(grep 'TopicManager' $CPP_FILES | wc -l)
		local LONG_ONE_NO=$(grep 'SingleSubscriberPublisher' $CPP_FILES | wc -l)

		echo "$PACKAGE,$NH_NO,$NH_ARGS_NO,$ADVERTISE_NO,$BARE_PUB_NO,$PUBLICATION_NO,$TOPICMANAGER_NO,$LONG_ONE_NO" >> "$RESULTS_FILE"
	done
}


function publish-subscribe-same-file {
	echo 'Package,File,Subscribe,Publish' >> "$RESULTS_FILE"

    while read PACKAGE HASH VCS URL; do
        PACKAGE_DIR="$(get-package "$PACKAGE" "$HASH" "$VCS" "$URL")"

		echo "Analysing package $PACKAGE"

		for FILE in $(find "$PACKAGE_DIR" -type f); do
			local TYPE=$(file -b "$FILE")
			if grep -P '^C.*source' <<<"$TYPE" &> /dev/null; then
				local CPP_PUBLISH_NO=$(grep '\.publish' "$FILE" | wc -l)
				local CPP_SUBSCRIBE_NO=$(grep '\.subscribe' "$FILE" | wc -l)

				echo "$PACKAGE,$FILE,$CPP_PUBLISH_NO,$CPP_SUBSCRIBE_NO" >> "$RESULTS_FILE"
			elif grep -P '^Python' <<<"$TYPE" &> /dev/null; then
				local PY_PUBLISH_NO=$(grep 'Publisher' "$FILE" | wc -l)
				local PY_SUBSCRIBE_NO=$(grep 'Subscriber' "$FILE" | wc -l)

				echo "$PACKAGE,$FILE,$PY_PUBLISH_NO,$PY_SUBSCRIBE_NO" >> "$RESULTS_FILE"
			fi
		done
	done
}


function packages-composition {
    cat > "$RESULTS_FILE" <<SQLEOF
create table metadata (          -- http://cloc.sourceforge.net v 1.60
                timestamp text,
                Project   text,
                elapsed_s real);
create table t        (
                Project   text   ,
                Language  text   ,
                File      text   ,
                nBlank    integer,
                nComment  integer,
                nCode     integer,
                nScaled   real   );
SQLEOF

    while read PACKAGE HASH VCS URL; do
        PACKAGE_DIR="$(get-package "$PACKAGE" "$HASH" "$VCS" "$URL")"

		cloc "$PACKAGE_DIR" --sql="$RESULTS_FILE" --sql-project="$PACKAGE" --sql-append
	done
}

"$STAT_FUNCTION"
